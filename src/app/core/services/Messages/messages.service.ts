import { IMessageContent } from '../../interfaces/imessage-content';
import { IMessages } from '../../interfaces/imessages';
import { Injectable } from '@angular/core';
import { LS_MESSAGES } from '../../constants/Storage.constants';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  // Array for list of messages
  private messages: IMessages[] = [];
  constructor() { }

  // Methods of Messages Service
  saveMessage(messageContent: IMessageContent): void {
    // Push new message in messages array
    this.messages.push({
      messageContent, // Message Content have: Name, Phone, Email and Project Brief
      status: false,
      create: new Date(),
    })

    // Update localstorage
    this.saveInLocalStorage();
  }

  getMessages(): IMessages[] {
    return this.messages;
  }

  getTotalMessages(): number {
    return this.messages.length
  }

  //* Remove message of array
  deleteMessage(index: number): void {
    this.messages.splice(index,1);
    // Update in localstorage array
    this.saveInLocalStorage();
  }

  //* Save/Update Array of messages in localstorage
  saveInLocalStorage(): void {
    localStorage.setItem(LS_MESSAGES, JSON.stringify(this.messages));
  }
  //* Read Array saved in localstorage
  readOfLocalStorage(): void {
    // get messages of localstorage with key LS_MESSAGES
    const messagesInString = localStorage.getItem(LS_MESSAGES);
    // Validate if localstorage is not empty of LS_MESSAGES exist
    if (!messagesInString) return
    this.messages = JSON.parse(messagesInString);
  }
}
