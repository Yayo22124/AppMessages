import { IMessageContent } from './imessage-content';

export interface IMessages {
    messageContent: IMessageContent,
    status: boolean,
    create: Date
}
