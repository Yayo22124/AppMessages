export interface IMessageContent {
    name: string,
    phone: string,
    email: string,
    projectBrief: string
}
