import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion, MatExpansionModule } from '@angular/material/expansion';

import { IMessages } from '../../../../core/interfaces/imessages';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MessagesService } from '../../../../core/services/Messages/messages.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-received-messages',
  standalone: true,
  imports: [MatExpansionModule, MatButtonModule, MatIconModule, RouterLink, MatDividerModule],
  templateUrl: './received-messages.component.html',
  styleUrl: './received-messages.component.scss',
})
export class ReceivedMessagesComponent implements OnInit {
  constructor(
    public messagesService: MessagesService,
    private _snackBar: MatSnackBar
  ) {}
  ArrayMessages: IMessages[] = this.messagesService.getMessages();
  deleteMessage(index: number): void {
    if (index < 0){
      this._snackBar.open("No se puede eliminar un mensaje que no existe", "Cerrar", {
        duration: 5*1000
      })
      return;
    }
    this._snackBar.open("Mensaje Eliminado", "Cerrar", {
      duration: 5*1000
    })
    this.messagesService.deleteMessage(index)
    
  }

  updateStatusMessage(): void {
    this.messagesService.saveInLocalStorage();
  }

  ngOnInit(): void {
    this.ArrayMessages = this.messagesService.getMessages();
  }
}
