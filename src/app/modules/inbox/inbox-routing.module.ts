import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { ReceivedMessagesComponent } from './pages/received-messages/received-messages.component';

const routes: Routes = [
  {
    path: "",
    component: ReceivedMessagesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InboxRoutingModule { }
