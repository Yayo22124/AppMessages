import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSendProjectComponent } from './form-send-project.component';

describe('FormSendProjectComponent', () => {
  let component: FormSendProjectComponent;
  let fixture: ComponentFixture<FormSendProjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormSendProjectComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FormSendProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
