import { Component, inject } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  MatSnackBar,
  MatSnackBarAction,
  MatSnackBarActions,
  MatSnackBarLabel,
  MatSnackBarModule,
  MatSnackBarRef,
} from '@angular/material/snack-bar';

import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MessagesService } from '../../../../core/services/Messages/messages.service';

@Component({
  selector: 'app-form-send-project',
  standalone: true,
  imports: [
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FormsModule,
    MatSnackBarModule,
  ],
  templateUrl: './form-send-project.component.html',
  styleUrl: './form-send-project.component.scss',
})
export class FormSendProjectComponent {
  constructor(
    private fb: FormBuilder,
    private messageService: MessagesService,
    private _snackBar: MatSnackBar
  ) {}
  formMessage: FormGroup = this.fb.group({
    name: [
      '',
      [Validators.minLength(3), Validators.required, Validators.maxLength(50)],
    ],
    phone: [
      '',
      [
        Validators.pattern(
          '^[+]?[(]?[0-9]{3}[)]?[-s.]?[0-9]{3}[-s.]?[0-9]{4,6}$'
        ),
        Validators.required,
      ],
    ],
    email: ['', [Validators.email, Validators.required]],
    projectBrief: [
      '',
      [
        Validators.minLength(10),
        Validators.required,
        Validators.maxLength(250),
      ],
    ],
  });

  // Save Message using MessagesService
  saveMessage() {
    if (!this.formMessage.invalid) {
      // Save message in local storage using the service
      this.messageService.saveMessage(this.formMessage.value);
      // reset fields of form
      this.formMessage.reset();
      this._snackBar.openFromComponent(NotificationComponent, {
        duration: 5*1000
      })
    }
  }
}

@Component({
  selector: 'snack-bar-annotated-component-example-snack',
  template: `
    <span class="snackbar-text" matSnackBarLabel> Proyecto Enviado </span>
    <span matSnackBarActions>
      <button
        class="snackbar-button"
        mat-button
        matSnackBarAction
        (click)="snackBarRef.dismissWithAction()"
      >
        Cerrar
      </button>
    </span>
  `,
  styles: [
    `
      :host {
        display: flex;
      }

      .snackbar-text {
        color: white;
        //font-weight: bold;
      }
      .snackbar-button {
        color: white !important;
        background: #A8845f !important;
      }
    `,
  ],
  standalone: true,
  imports: [
    MatButtonModule,
    MatSnackBarLabel,
    MatSnackBarActions,
    MatSnackBarAction,
  ],
})
export class NotificationComponent {
  snackBarRef = inject(MatSnackBarRef);
}
