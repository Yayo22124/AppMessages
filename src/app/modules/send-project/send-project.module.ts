import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SendProjectRoutingModule } from './send-project-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SendProjectRoutingModule
  ]
})
export class SendProjectModule { }
