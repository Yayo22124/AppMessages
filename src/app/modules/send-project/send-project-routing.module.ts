import { RouterModule, Routes } from '@angular/router';

import { FormSendProjectComponent } from './pages/form-send-project/form-send-project.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: "",
    component: FormSendProjectComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SendProjectRoutingModule { }
