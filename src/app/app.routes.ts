import { HomeModule } from './modules/home/home.module';
import { InboxModule } from './modules/inbox/inbox.module';
import { Routes } from '@angular/router';
import { SendProjectModule } from './modules/send-project/send-project.module';

export const routes: Routes = [
    {
        path: "",
        redirectTo: "home",
        pathMatch: 'full'
    },
    { //* Home
        path: "home",
        loadChildren: () => import("./modules/home/home.module").then(home => HomeModule)
    },
    { //* Inbox
        path: "inbox",
        children: [
            { //* Received Messages
                path: "messages",
                loadChildren: () => import("./modules/inbox/inbox.module").then(inbox => InboxModule)
            }
        ]
    },
    { //* Send
        path: "send",
        children: [
            { //* Send a Message
                path: "message",
                loadChildren: () => import("./modules/send-project/send-project.module").then(send => SendProjectModule)
            }
        ]
    }
];
